#include "stdafx.h"
#include "BlackjackHand.h"


BlackjackHand::BlackjackHand()
{
}

void BlackjackHand::setPossibleHandValues()
{
	//Attempts to handle cases where the "Ace" card can be either one or eleven.
	possibleHandValues[0] = 0;
	possibleHandValues[1] = 0;

	for (auto& card : hand)
	{
		if (card.getFace() == "ace")
		{
			possibleHandValues[0] += 1;
			possibleHandValues[1] += 11;
		}

		if (card.getFace() == "two")
		{
			possibleHandValues[0] += 2;
			possibleHandValues[1] += 2;
		}

		if (card.getFace() == "three")
		{
			possibleHandValues[0] += 3;
			possibleHandValues[1] += 3;
		}

		if (card.getFace() == "four")
		{
			possibleHandValues[0] += 4;
			possibleHandValues[1] += 4;
		}

		if (card.getFace() == "five")
		{
			possibleHandValues[0] += 5;
			possibleHandValues[1] += 5;
		}

		if (card.getFace() == "six")
		{
			possibleHandValues[0] += 6;
			possibleHandValues[1] += 6;
		}

		if (card.getFace() == "seven")
		{
			possibleHandValues[0] += 7;
			possibleHandValues[1] += 7;
		}

		if (card.getFace() == "eight")
		{
			possibleHandValues[0] += 8;
			possibleHandValues[1] += 8;
		}

		if (card.getFace() == "nine")
		{
			possibleHandValues[0] += 9;
			possibleHandValues[1] += 9;
		}

		if (card.getFace() == "ten")
		{
			possibleHandValues[0] += 10;
			possibleHandValues[1] += 10;
		}

		if (card.getFace() == "jack")
		{
			possibleHandValues[0] += 10;
			possibleHandValues[1] += 10;
		}

		if (card.getFace() == "queen")
		{
			possibleHandValues[0] += 10;
			possibleHandValues[1] += 10;
		}

		if (card.getFace() == "king")
		{
			possibleHandValues[0] += 10;
			possibleHandValues[1] += 10;
		}
	}
}

bool BlackjackHand::isOverTwentyOne()
{
	setPossibleHandValues();
	return (isHandValueOverTwentyOne(possibleHandValues[0]) && isHandValueOverTwentyOne(possibleHandValues[1]));	
}

bool BlackjackHand::isHandValueOverTwentyOne(int handValue)
{
	return (handValue > 21);
}


bool BlackjackHand::IsNaturalBlackJack()
{
	/*Only return true when the starting hand of two contains the value of 21.  
	 *setPossibleValues() sets the ace card to be 11 only in possibleHandValues[1].
	 */
	setPossibleHandValues();
	return (possibleHandValues[1] == 21 && hand.size() == 2);
}

int BlackjackHand::getValue()
{
	setPossibleHandValues();
	if (possibleHandValues[0] != possibleHandValues[1] && possibleHandValues[1] < 21)
		return possibleHandValues[1];
	else
		return possibleHandValues[0];
}

