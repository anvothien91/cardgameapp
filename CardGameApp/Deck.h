#pragma once
#include "Card.h"
#include <array>
struct Deck
{
public:
	Deck();	
	Deck(int num);
	void fill();
	void printDeckContents();
	void addCard(std::string suit, std::string face);	

private:	
	Card *head;
	Card *tail;
	int deckSize;
	std::array<std::string, 4> suits{ "heart", "club", "diamond", "spade" };
	std::array<std::string, 13> faces{ "ace", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "jack", "queen", "king" };
};

