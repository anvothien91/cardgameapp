#pragma once
#include "BlackjackHand.h"
class BlackjackDealerHand :
	public BlackjackHand
{
public:
	BlackjackDealerHand();
	~BlackjackDealerHand();
	void displayAllButFirstCard();
	void displayAllCards();
};

