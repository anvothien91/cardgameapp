#pragma once
#include<algorithm>
#include<string>
#include<ctype.h>
#include<locale>


class Card
{
public:
	Card();
	Card(std::string suit, std::string face);
	enum Suit {HEART, CLUB, DIAMOND, SPADE};
	enum Face {ACE,TWO,THREE,FOUR,FIVE,SIX,SEVEN,EIGHT,NINE,TEN,JACK,QUEEN,KING};
	Card(Suit, Face);
	std::string getFace();
	std::string getSuit();
	Card* next = NULL;

private:
	Card::Suit  _suit;
	Card::Face _face;
	void setSuit(std::string suit);
	void setFace(std::string face);
};

