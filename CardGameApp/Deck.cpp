#include "stdafx.h"
#include "Deck.h"
#include <iostream>
#include <array>
using namespace std;

Deck::Deck()
{
	head = NULL;
	tail = NULL;
	deckSize = 0;
	fill();
}

Deck::Deck(int num)
{
	head = NULL;
	tail = NULL;
	deckSize = 0;
	for (int i = 1; i <= num; i++)
	{
		fill();
	}
}

void Deck::fill()
{	
	for (auto& suit : suits)
	{
		for (auto& face : faces)
		{
			addCard(suit, face);
		}
	}
}

void Deck::printDeckContents()
{
	Card *temp = new Card();
	temp = head;
	cout << "Current Deck Count: " << deckSize << endl;

	while (temp != NULL)
	{
		cout << temp->getSuit() << " of " << temp->getFace() << endl;
		temp = temp->next;
	}
}

void Deck::addCard(string suit, string face)
{
	Card *temp = new Card(suit, face);
	temp->next = NULL;

	if (head == NULL)
	{
		head = temp;
		tail = temp;
		temp = NULL;
	}
	else
	{
		tail->next = temp;
		tail = temp;
	}
	deckSize++;
}
