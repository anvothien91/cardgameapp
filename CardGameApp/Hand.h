#pragma once
#include "Card.h"
#include <vector>
class Hand
{
protected:
	std::vector<Card> hand;
public:
	Hand();
	void clearHand();
	void addCard(Card card);
	int length();
	void displayHand();
};

