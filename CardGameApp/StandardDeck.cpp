#include "stdafx.h"
#include "StandardDeck.h"
#include <vector>
#include <list>
#include <algorithm>
#include <random>
#include <chrono>
#include <iostream>


StandardDeck::StandardDeck()
{
	fill();
}

StandardDeck::StandardDeck(int n)
{
	for (int i = 1; i <= n; i++)
	{
		fill();
	}
}

void StandardDeck::fill()
{
	for (auto& suit : suits)
	{
		for (auto& face : faces)
		{
			addCard(suit, face);
		}
	}
}

void StandardDeck::checkForEmptyDeck()
{
	if (deck.empty())
	{
		fill();
	}
}

void StandardDeck::pop()
{
	deck.pop_back();
}

Card StandardDeck::peek()
{
	return deck.back();
}

void StandardDeck::addCard(std::string suit, std::string face)
{
	deck.push_back(Card(suit,face));
}

void StandardDeck::shuffle()
{
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(deck.begin(), deck.end(), std::default_random_engine(seed));
}

void StandardDeck::printDeckContents()
{
	std::cout << "Current Deck Count: " << deck.size() << std::endl;

	for (auto& card : deck)
	{
		std::cout << card.getSuit() << " of " << card.getFace() << std::endl;
	}
}


