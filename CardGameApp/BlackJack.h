#pragma once
#include "BlackjackDealer.h"
#include "Game.h"
#include "StandardDeck.h"
#include "BlackjackPlayer.h"
#include <algorithm>
#include <vector>
#include <map>
class BlackJack :
	public Game
{
public:
	BlackJack();
	void play() override;

private:
	int numOfPlayers;	
	void promptForNumberOfPlayers();
	void setNumOfPlayers(int n);
	void initializePlayers();
	void startRound();
	void checkForNaturalBlackJack();
	void closeOutRound(BlackjackDealer dealer, std::vector<BlackjackPlayer> &players);
	void payWinners(BlackjackDealer dealer, std::vector<BlackjackPlayer> &players);
	bool dealerHasNaturalBlackJack(BlackjackDealer dealer);
	void startThePlay();
	void addCardToPlayerHand(BlackjackPlayer &player);
	void startDealerPlay(BlackjackDealer &dealer);
	void takeBets();
	void printSeperator();
	void dealCardsToEveryone();
	void printCurrentMoneyStatus(std::vector<BlackjackPlayer> players);
	std::map<int, int> betValueMap;
	StandardDeck deck = StandardDeck(4);
	void printCurrentGameStatus();
	BlackjackDealer dealer;
	std::vector<BlackjackPlayer> players;
};

