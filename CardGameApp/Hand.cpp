#include "stdafx.h"
#include "Hand.h"
#include <iostream>


Hand::Hand()
{
}

void Hand::clearHand()
{
	hand.clear();
}

void Hand::addCard(Card card)
{
	hand.push_back(card);
}

int Hand::length()
{
	return(hand.size());
}
void Hand::displayHand()
{
	std::cout << "Current Hand Count: " << hand.size() << std::endl;

	for (auto& card : hand)
	{
		std::cout << card.getSuit() << " of " << card.getFace() << std::endl;
	}
}
