#include "stdafx.h"
#include "BlackjackDealerHand.h"
#include <iostream>

BlackjackDealerHand::BlackjackDealerHand()
{
}


BlackjackDealerHand::~BlackjackDealerHand()
{
}

void BlackjackDealerHand::displayAllButFirstCard()
{
	std::cout << "Dealer's hand" << std::endl;
	std::cout << "Dealer has one hidden card" << std::endl;
	std::cout << "Revealed cards \n\n";

	for (auto& card : hand)
	{
		//Dealer hides first card from being displayed to blackjack players
		if (&card == &hand.front()) { continue; }

		else
		{
			std::cout << card.getSuit() << " of " << card.getFace() << std::endl;
		}
	}
}

void BlackjackDealerHand::displayAllCards()
{
	std::cout << "Dealer's hand" << std::endl;
	for (auto& card : hand)
	{
		std::cout << card.getSuit() << " of " << card.getFace() << std::endl;
	}
}
