#pragma once
#include "Deck.h"
#include <list>
#include <algorithm>
#include <vector>

class StandardDeck :
	public Deck
{

private:
	std::vector<Card> deck;
	std::array<std::string, 4> suits{ "heart", "club", "diamond", "spade" };
	std::array<std::string, 13> faces{ "ace", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "jack", "queen", "king" };

public:
	StandardDeck();
	StandardDeck(int n);
	void fill();
	void checkForEmptyDeck();
	void pop();
	Card peek();
	void addCard(std::string suit, std::string face);
	void shuffle();
	void printDeckContents();
};

