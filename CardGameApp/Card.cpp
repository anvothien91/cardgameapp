#include "stdafx.h"
#include "Card.h"
#include <algorithm>
#include <string>
#include <locale>

Card::Card()
{

}

Card::Card(std::string suit, std::string face)
{
	setSuit(suit);
	setFace(face);
}

Card::Card(Card::Suit s, Card::Face v)
{
	Card::_suit = s;
	Card::_face = v;
}

std::string Card::getSuit()
{
	switch (_suit)
	{
		case Suit::HEART:
			return "heart";
		case Suit::CLUB:
			return "club";
		case Suit::DIAMOND:
			return "diamond";
		case Suit::SPADE:
			return "spade";
		default:
			throw "invalid suit";
	}
}

std::string Card::getFace()
{
	switch (_face)
	{
		case Face::ACE:
			return "ace";
		case Face::TWO:
			return "two";
		case Face::THREE:
			return "three";
		case Face::FOUR:
			return "four";
		case Face::FIVE:
			return "five";
		case Face::SIX:
			return "six";
		case Face::SEVEN:
			return "seven";
		case Face::EIGHT:
			return "eight";
		case Face::NINE:
			return "nine";
		case Face::TEN:
			return "ten";
		case Face::JACK:
			return "jack";
		case Face::QUEEN:
			return "queen";
		case Face::KING:
			return "king";
		default:
			throw "invalid face";		
	}
}

void Card::setSuit(std::string suit)
{
	std::transform(suit.begin(), suit.end(), suit.begin(), ::tolower);

	if (suit == "heart")
		_suit = Suit::HEART;
	else if (suit == "club")
		_suit = Suit::CLUB;
	else if (suit == "diamond")
		_suit = Suit::DIAMOND;
	else if (suit == "spade")
		_suit = Suit::SPADE;
	else
		throw "invalid string to initialize _suit";
}

void Card::setFace(std::string face)
{
	std::transform(face.begin(), face.end(), face.begin(), ::tolower);

	if (face == "ace")
		_face = Face::ACE;
	else if (face == "two")
		_face = Face::TWO;
	else if (face == "three")
		_face = Face::THREE;
	else if (face == "four")
		_face = Face::FOUR;
	else if (face == "five")
		_face = Face::FIVE;
	else if (face == "six")
		_face = Face::SIX;
	else if (face == "seven")
		_face = Face::SEVEN;
	else if (face == "eight")
		_face = Face::EIGHT;
	else if (face == "nine")
		_face = Face::NINE;
	else if (face == "ten")
		_face = Face::TEN;
	else if (face == "jack")
		_face = Face::JACK;
	else if (face == "queen")
		_face = Face::QUEEN;
	else if (face == "king")
		_face = Face::KING;
	else
		throw "invalid string to initialize _face";
}
