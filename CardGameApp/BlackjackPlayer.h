#pragma once
#include "BlackjackHand.h"

class BlackjackPlayer
{
public:
	BlackjackPlayer();
	BlackjackPlayer(int id);
	BlackjackHand hand;
	int getPlayerId();
	int getMoney();
	void subtractMoney(int amount);
	void addMoney(int amount);
	void setIsActiveInRound(bool activeStatus);
	void setHasWonRound(bool roundStatus);
	bool getIsActiveInRound();

private: 	
	int playerId;
	int money;
	bool isActiveInRound = true;
	bool hasWonRound = false;	
};

