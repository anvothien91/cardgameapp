#include "stdafx.h"
#include "BlackJack.h"
#include <iostream>
#include <string>

BlackJack::BlackJack()
{
}

void BlackJack::play()
{
	std::cout << "Welcome to BlackJack" << std::endl;
	promptForNumberOfPlayers();
	dealer = BlackjackDealer();
	initializePlayers();
	deck.shuffle();
	startRound();	
}

void BlackJack::initializePlayers()
{
	for (int i = 1; i <= numOfPlayers; i++)
	{
		BlackjackPlayer player = BlackjackPlayer(i);
		players.push_back(player);
	}
}

void BlackJack::startRound()
{
	printCurrentMoneyStatus(players);
	takeBets();
	dealCardsToEveryone();
	checkForNaturalBlackJack();
	if (!dealerHasNaturalBlackJack(dealer))
	{		
		startThePlay();
		startDealerPlay(dealer);
	}
	closeOutRound(dealer, players);	
}

void BlackJack::checkForNaturalBlackJack()
{

	for (auto& player : players)
	{
		if(player.hand.IsNaturalBlackJack())
		{ 
			player.setIsActiveInRound(false);
		}
	}
}

void BlackJack::closeOutRound(BlackjackDealer dealer, std::vector<BlackjackPlayer> &players)
{	
	printCurrentGameStatus();
	payWinners(dealer, players);
	printCurrentMoneyStatus(players);
}

void BlackJack::payWinners(BlackjackDealer dealer, std::vector<BlackjackPlayer> &players)
{
	for (auto& player : players)
	{
		if (player.hand.isOverTwentyOne())
		{
			continue;
		}
		else if (player.hand.IsNaturalBlackJack() && dealer.hand.IsNaturalBlackJack() || (player.hand.getValue() == dealer.hand.getValue()))
		{
			player.addMoney(betValueMap[player.getPlayerId()]);
		}
		else if (!player.hand.IsNaturalBlackJack() && dealer.hand.IsNaturalBlackJack())
		{
			continue;
		}
		else if (player.hand.IsNaturalBlackJack() && !dealer.hand.IsNaturalBlackJack())
		{
			std::cout << "Player " << player.getPlayerId() << " has won off having a natural blackjack!" << std::endl;
			player.addMoney(betValueMap[player.getPlayerId()] * 2);
			std::cout << "New player balance: " << std::to_string(player.getMoney()) << std::endl;
		}
		else if (player.hand.getValue() > dealer.hand.getValue() && !player.hand.isOverTwentyOne())
		{
			std::cout << "Player " << player.getPlayerId() << " has won" << std::endl;
			player.addMoney(betValueMap[player.getPlayerId()] * 2);
			std::cout << "New player balance: " << std::to_string(player.getMoney()) << std::endl;
		}
		else if (dealer.hand.isOverTwentyOne() && !player.hand.isOverTwentyOne())
		{
			std::cout << "Player " << player.getPlayerId() << " has won" << std::endl;
			player.addMoney(betValueMap[player.getPlayerId()] * 2);
			std::cout << "New player balance: " << std::to_string(player.getMoney()) << std::endl;
		}
		else
		{
			continue;
		}	
	}
}

bool BlackJack::dealerHasNaturalBlackJack(BlackjackDealer dealer)
{
	return(dealer.hand.getValue() == 21 && dealer.hand.length() == 2);
}

void BlackJack::startThePlay()
{
	dealer.hand.displayAllButFirstCard();
	printSeperator();
	for (auto& player : players)
	{
		while (true)
		{
			//IsActiveInRound should break here if player got a natural blackjack during checkForNaturalBlackJack().
			if (!player.getIsActiveInRound()) { break; }

			std::string userInput;

			//TODO: add logic to handle situations where players can opt to split.
			printSeperator();
			std::cout << "Player " << player.getPlayerId() << "'s turn" << std::endl;
			player.hand.displayHand();
			std::cout << "Current Hand Value: " << std::to_string(player.hand.getValue()) << std::endl;
			printSeperator();
			std::cout << "Type hit or stand" << std::endl;
			std::getline(std::cin, userInput);
			std::transform(userInput.begin(), userInput.end(), userInput.begin(), ::tolower);
			if (userInput == "hit")
			{
				addCardToPlayerHand(player);
				if (player.hand.isOverTwentyOne())
				{
					player.hand.displayHand();
					std::cout << "Player " << player.getPlayerId() << " went over 21" << std::endl;
					player.setHasWonRound(false);
					player.setIsActiveInRound(false);
					break;
				}
			}
			else if (userInput == "stand")
			{
				break;
			}
			else
			{
				std::cout << "Invalid command" << std::endl;
			}
			printSeperator();
		}
	}
}

void BlackJack::addCardToPlayerHand(BlackjackPlayer &player)
{
	deck.checkForEmptyDeck();
	player.hand.addCard(deck.peek());
	deck.pop();
}

void BlackJack::startDealerPlay(BlackjackDealer &dealer)
{
	if (dealer.hand.IsNaturalBlackJack())
		return;
	else
	{
		while (dealer.hand.getValue() < 17)
		{
			deck.checkForEmptyDeck();
			dealer.hand.addCard(deck.peek());
			deck.pop();			
		}
	}
}

void BlackJack::takeBets()
{
	for (auto& player : players)
	{
		std::string userInput;

		while (true)
		{
			std::cout << "Player" << std::to_string(player.getPlayerId()) << " has $" << std::to_string(player.getMoney()) << std::endl;
			std::cout << "Place your bet: " << std::endl;
			std::getline(std::cin, userInput);
			try
			{
				int num = std::stoi(userInput);
				if (num <= player.getMoney())
				{
					betValueMap[player.getPlayerId()] = num;
					player.subtractMoney(num);
					break;
				}
				else
				{
					std::cout << "Player does not have enough money to bet that amount" << std::endl;
				}

			}
			catch(std::invalid_argument)
			{
				std::cout << "Invalid argument for bet";
			}
			
		}
	}
}

void BlackJack::printSeperator()
{
	std::cout << "------------------------------------------------" << std::endl;
}

void BlackJack::dealCardsToEveryone()
{
	int i = 1;
	while (i <= 2)
	{
		for (auto& player : players)
		{
			deck.checkForEmptyDeck();
			player.hand.addCard(deck.peek());
			deck.pop();
		}

		deck.checkForEmptyDeck();
		dealer.hand.addCard(deck.peek());
		deck.pop();
		i++;
	}
}

void BlackJack::printCurrentMoneyStatus(std::vector<BlackjackPlayer> players)
{
	for (auto& player : players)
	{
		std::cout << "Player " << std::to_string(player.getPlayerId()) << " has a balance of $" << std::to_string(player.getMoney()) << std::endl;
	}
}

void BlackJack::printCurrentGameStatus()
{
	std::cout << "================================================" << std::endl;
	std::cout << "Current Game Status " << std::endl;
	std::cout << "Number of players: " << std::to_string(numOfPlayers) << std::endl;
	std::cout << "------------------------------------------------" << std::endl;
	for (auto& player : players)
	{
		std::cout << "Player " << std::to_string(player.getPlayerId()) << "'s hand" << std::endl;
		std::cout << "Hand Value: " << std::to_string(player.hand.getValue()) << std::endl;
		player.hand.displayHand();
		std::cout << "Money: " << std::to_string(player.getMoney()) << std::endl;
		std::cout << "------------------------------------------------" << std::endl;
	}

	std::cout << "Dealer's hand" << std::endl;
	std::cout << "Hand Value " << std::to_string(dealer.hand.getValue()) << std::endl;
	dealer.hand.displayAllCards();
	printSeperator();
}

void BlackJack::promptForNumberOfPlayers()
{
	std::string userInput;
	std::cout << "How many people are playing? (Enter a number from 1-4): ";
	std::getline(std::cin, userInput);	
	try
	{
		int num = std::stoi(userInput);
		if (num > 0 && num <= 4)
			setNumOfPlayers(num);
		else
			std::cout << "Number of players can only be 1,2,3,4";
	}
	catch (std::invalid_argument)
	{
		std::cout << "Invalid argument for number of players.";
	}
	catch (std::out_of_range)
	{
		std::cout << "Out of range int.";
	}
}

void BlackJack::setNumOfPlayers(int n)
{
	numOfPlayers = n;
}

