#pragma once
#include "Hand.h"
class BlackjackHand :
	public Hand
{
public:
	BlackjackHand();
	bool isOverTwentyOne();
	bool isHandValueOverTwentyOne(int handValue);
	bool IsNaturalBlackJack();
	int getValue();

private:
	int possibleHandValues[2];
	void setPossibleHandValues();

};

