#include "stdafx.h"
#include "BlackjackPlayer.h"


BlackjackPlayer::BlackjackPlayer()
{
	//WARNING - Should use BlackjackPlayer(int id) unless you have get/set methods for playerId.
	hand = BlackjackHand();
}

BlackjackPlayer::BlackjackPlayer(int id)
{
	playerId = id;
	money = 1000;
	hand = BlackjackHand();
}

int BlackjackPlayer::getPlayerId()
{
	return playerId;
}

int BlackjackPlayer::getMoney()
{
	return money;
}

void BlackjackPlayer::subtractMoney(int amount)
{
	money = money -= amount;
}

void BlackjackPlayer::addMoney(int amount)
{
	money = money += amount;
}

void BlackjackPlayer::setIsActiveInRound(bool activeStatus)
{
	isActiveInRound = activeStatus;
}

void BlackjackPlayer::setHasWonRound(bool roundStatus)
{
	hasWonRound = roundStatus;
}

bool BlackjackPlayer::getIsActiveInRound()
{
	return isActiveInRound;
}



